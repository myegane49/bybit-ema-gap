from os import environ
from math import fabs 
from time import mktime, strptime, gmtime, strftime

from pybit import usdt_perpetual 
from dotenv import load_dotenv
from jinja2 import Environment, FileSystemLoader

load_dotenv()
env = Environment(loader = FileSystemLoader('./'))
template = env.get_template("template.html")

pair = 'BTCUSDT'

session = usdt_perpetual.HTTP(
    endpoint=environ.get('ENDPOINT'),
    api_key=environ.get('API_KEY'),
    api_secret=environ.get('API_SECRET')
)

start_str = '2022-07-01'
end_str = '2022-08-01'
start = mktime(strptime(start_str, "%Y-%m-%d"))
end = mktime(strptime(end_str, "%Y-%m-%d"))

#  records_exists = True
#  page_n = 1
#  wallet_records = []
#  while records_exists:
#      records = session.wallet_fund_records(start_date=start_str, end_date=end_str, currency='USDT', page=page_n, limit=50)['result']['data']
#      if records != None:
#          wallet_records.extend(records)
#          page_n += 1
#      else:
#          records_exists = False

data_exists = True
page_num = 1
all_positions = []
while data_exists:
    positions = session.closed_profit_and_loss(symbol=pair, start_time=start, end_time=end, page=page_num, limit=50)['result']['data']
    if positions != None:
        all_positions.extend(positions)
        page_num += 1
    else:
        data_exists = False

won_positions = [pos for pos in all_positions if pos['closed_pnl'] > 0]
lost_positions = [pos for pos in all_positions if pos['closed_pnl'] < 0]
long_positions = [pos for pos in all_positions if pos['side'] == 'Sell']
short_positions = [pos for pos in all_positions if pos['side'] == 'Buy']

total_num = len(all_positions)
won_num = len(won_positions)
lost_num = len(lost_positions)

total_won_value = 0
for pos in won_positions:
    total_won_value += pos['closed_pnl']
total_won_value = round(total_won_value, 2)
total_lost_value = 0
for pos in lost_positions:
    total_lost_value += pos['closed_pnl']
total_lost_value = round(total_lost_value, 2)
net_profit = round(total_lost_value + total_won_value, 2)

avg_won_value = '-'
if won_num != 0:
    avg_won_value = round(total_won_value / won_num, 2)
avg_lost_value = '-'
if lost_num != 0:
    avg_lost_value = round(total_lost_value / lost_num, 2)
avg_any_value = '-'
if total_num != 0:
    avg_any_value = round(net_profit / total_num, 2)

winning_ratio = '-'
if total_num != 0:
    winning_ratio = round(won_num / total_num * 100, 2)
profit_per_loss = '-'
if avg_won_value != '-' and avg_lost_value != '-':
    profit_per_loss = round(avg_won_value / fabs(avg_lost_value), 2)
profit_factor = '-'
if total_lost_value != 0:
    profit_factor = round(total_won_value / total_lost_value, 4)

max_won = 0
for pos in won_positions:
    if pos['closed_pnl'] > max_won:
        max_won = pos['closed_pnl']
max_won = round(max_won, 2)
max_lost = 0
for pos in lost_positions:
    if pos['closed_pnl'] < max_lost:
        max_lost = pos['closed_pnl']
max_lost = round(max_lost, 2)

short_num = len(short_positions)
long_num = len(long_positions)

won_short_positions = [pos for pos in short_positions if pos['closed_pnl'] > 0]
won_long_positions = [pos for pos in long_positions if pos['closed_pnl'] < 0]
short_winning_ratio = '-'
if short_num != 0:
   short_winning_ratio = round(len(won_short_positions) / short_num * 100, 2) 
long_winning_ratio = '-'
if long_num != 0:
   long_winning_ratio = round(len(won_long_positions) / long_num * 100, 2) 

converted_positions = []
num = 1
for pos in all_positions:
    side = 'Buy'
    if pos['side'] == 'Buy':
        side = 'Sell'
    new_pos = {
        'num': num,
        'side': side,
        'quantity': pos['qty'],
        'entry_price': pos['avg_entry_price'],
        'exit_price': pos['avg_exit_price'],
        'pnl': pos['closed_pnl'],
        'created_at': strftime("%Y-%m-%d %H:%M", gmtime(pos['created_at']))
    }
    num += 1
    converted_positions.append(new_pos)

curr_balance = 10000
balance_list = [curr_balance]
for pos in converted_positions:
    curr_balance = round(curr_balance + pos['pnl'])
    balance_list.append(curr_balance)

with open('report.html', mode="w") as f:
    rendered_data = template.render(
        pair=pair,
        total_num=total_num, won_num=won_num, lost_num=lost_num,
        total_won_value=total_won_value, total_lost_value=total_lost_value, net_profit=net_profit,
        avg_won_value=avg_won_value, avg_lost_value=avg_lost_value, avg_any_value=avg_any_value,
        winning_ratio=winning_ratio, profit_per_loss=profit_per_loss, profit_factor=profit_factor,
        max_won=max_won, max_lost=max_lost,
        short_num=short_num, long_num=long_num,
        start_date=start_str, end_date=end_str,
        positions=converted_positions,
        balance_list=balance_list,
        short_winning_ratio=short_winning_ratio,
        long_winning_ratio=long_winning_ratio
    )
    f.write(rendered_data)
