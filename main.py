from math import fabs, trunc
from os import environ
from time import sleep, time

from talib.abstract import EMA, BBANDS
from pybit import usdt_perpetual 
from numpy import array
from dotenv import load_dotenv

load_dotenv()

pair = 'BTCUSDT'
leverage = 100
risk_per_trade = 0.02
size = None
period_resist = 75
period_signal = 4
signal_open_gap = 350
signal_close_gap = 80
bb_std = 5.0
bb_period = 20

session = usdt_perpetual.HTTP(
    endpoint=environ.get('ENDPOINT'),
    api_key=environ.get('API_KEY'),
    api_secret=environ.get('API_SECRET')
)
#  ws = usdt_perpetual.WebSocket(
#      test=True,
#      api_key=environ.get('API_KEY'),
#      api_secret=environ.get('API_SECRET')
#  )

try:
    session.set_leverage(symbol=pair, buy_leverage=leverage, sell_leverage=leverage)
except:
    pass

def calc_pos_size(max_loss_perc, entry_price, sl_price):
    acc_equity = session.get_wallet_balance(coin="USDT")['result']['USDT']['equity']
    loss_price_diff = fabs(entry_price - sl_price)
    max_loss_dollar = acc_equity * max_loss_perc
    pos_size = max_loss_dollar / loss_price_diff 
    return pos_size

from_timestamp = trunc(time()) - 75 * 5 * 60

while True:
    results = session.query_kline(
        symbol=pair,
        interval="5",
        from_time=from_timestamp
    )['result']
    closes = array([float(res["close"]) for res in results])

    ema_resist = EMA(closes, timeperiod=period_resist)[-1]
    ema_signal = EMA(closes, timeperiod=period_signal)[-1]
    gap = fabs(ema_resist - ema_signal)

    order = session.get_active_order(symbol=pair)
    if order['result']['data'] == None:
        if gap >= signal_open_gap and ema_resist > ema_signal:
            upper, middle, lower = BBANDS(closes, timeperiod=bb_period, nbdevup=bb_std, nbdevdn=bb_std)
            ask = session.latest_information_for_symbol(symbol=pair)['result'][0]['ask_price']
            size = calc_pos_size(risk_per_trade, ask, lower[-1])
            order_res = session.place_active_order(
                symbol=pair,
                side='Buy',
                order_type='Market',
                qty=size,
                price=ask,
                time_in_force='FillOrKill',
                reduce_only=False,
                close_on_trigger=False,
                stop_loss=lower[-1]
            )
            if order_res['ret_code'] != 0:
                print(order_res['ext_info'])
        elif gap >= signal_open_gap and ema_resist < ema_signal:
            upper, middle, lower = BBANDS(closes, timeperiod=bb_period, nbdevup=bb_std, nbdevdn=bb_std)
            bid = session.latest_information_for_symbol(symbol=pair)['result'][0]['bid_price']
            size = calc_pos_size(risk_per_trade, bid, upper[-1])
            order_res = session.place_active_order(
                symbol=pair,
                side='Sell',
                order_type='Market',
                qty=size,
                price=bid,
                time_in_force='FillOrKill',
                reduce_only=False,
                close_on_trigger=False,
                stop_loss=upper[-1]
            )
            if order_res['ret_code'] != 0:
                print(order_res['ext_info'])
    elif gap <= signal_close_gap and order['result']['data'][0]['side'] == 'Buy':
        bid = session.latest_information_for_symbol(symbol=pair)['result'][0]['bid_price']
        close_order_res = session.place_active_order(
            symbol=pair,
            side='Sell',
            order_type='Market',
            qty=size,
            price=bid,
            time_in_force='GoodTillCancel',
            reduce_only=True,
            close_on_trigger=False,
        )
        if close_order_res['ret_code'] != 0:
            print(close_order_res['ext_info'])
    elif gap <= signal_close_gap and order['result']['data'][0]['side'] == 'Sell':
        ask = session.latest_information_for_symbol(symbol=pair)['result'][0]['ask_price']
        close_order_res = session.place_active_order(
            symbol=pair,
            side='Buy',
            order_type='Market',
            qty=size,
            price=ask,
            time_in_force='GoodTillCancel',
            reduce_only=True,
            close_on_trigger=False,
        )
        if close_order_res['ret_code'] != 0:
            print(close_order_res['ext_info'])

    from_timestamp += 5 * 60
    sleep(5 * 60)

